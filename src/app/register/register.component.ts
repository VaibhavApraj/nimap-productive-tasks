import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators,FormGroup,FormBuilder} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { Router } from '@angular/router';
import { RegisterService } from '../sheard/register.service';

// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     const isSubmitted = form && form.submitted;
//     return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
//   }
// }

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public formGroup !:FormGroup;
  // emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  // passwordFormControl =new FormControl('', [Validators.required,]);

  // matcher = new MyErrorStateMatcher();
  constructor(private formBuilder:FormBuilder,private http:HttpClient,private router:Router,private registerService:RegisterService) { }

  ngOnInit(): void {
    this.formGroup=this.formBuilder.group({
      fname:['',[Validators.required]],
      lname:['',[Validators.required,]],
      email:['',[Validators.required, Validators.email]],
      password:['',[Validators.required,]],
      username:['',[Validators.required]],
      phone:['',[Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      gender:['',[Validators.required]],
      role:['',[Validators.required]]
    })
  }
  get fname ()
  {
    return this.formGroup.get('fname');
  }
  get lname ()
  {
    return this.formGroup.get('lname');
  }
  get email ()
  {
    return this.formGroup.get('email');
  }
  get password ()
  {
    return this.formGroup.get('password');
  }
  get username ()
  {
    return this.formGroup.get('username');
  }
  get phone ()
  {
    return this.formGroup.get('phone');
  }
  get gender ()
  {
    return this.formGroup.get('gender');
  }
  get role ()
  {
    return this.formGroup.get('role');
  }
  onSubmit(){
    console.log(this.formGroup.value);

    this.registerService.postData(this.formGroup.value).subscribe(
        (res) => {
          console.log(res);
          this.formGroup.reset();
          this.router.navigate(['/login']);
        },
        (err) => {
          console.log(err);
       },);

    // this.http.post<any>("http://localhost:3000/posts", this.formGroup.value)
    // .subscribe(
    //   (res) => {
    //     this.formGroup.reset();
    //     this.router.navigate(['/login']);
    //   },
    //   (err) => {
    //     console.log(err);
    //  },);
  }
}
