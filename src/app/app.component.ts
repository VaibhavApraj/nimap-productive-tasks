import { Component,OnInit } from '@angular/core';
import * as moment from 'moment';
import firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'authentication';
  ngOnInit(): void {
    const time = moment.utc();
    const firebaseConfig = {};
    firebase.initializeApp(firebaseConfig);
  }
}
