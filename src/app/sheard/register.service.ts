import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  readonly register="http://localhost:3000/posts";

  constructor(private http:HttpClient) { 
  }
  get()
  {
    return this.http.get(this.register);
  }
  postData(data : any)
  {
    return this.http.post<any>(this.register, data);
  }
  getData(id:number)
  {
    return this.http.get<any>("http://localhost:3000/posts/"+id)
    // .pipe(map((res:any)=>{
    //   return res;
    // }))
  }
}
