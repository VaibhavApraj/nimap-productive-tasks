import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators,FormGroup,FormBuilder} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { Router } from '@angular/router';
import {RegisterService} from '../sheard/register.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm !:FormGroup;
  loading = false;
  submitted = false;
  ifLogin: string;
  // emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  // passwordFormControl =new FormControl('', [Validators.required,]);

  matcher = new MyErrorStateMatcher();
  id: any;
  role: any;

  constructor(private formBuilder:FormBuilder,private http:HttpClient,private router:Router,private register :RegisterService) { }

  ngOnInit(): void {
    this.loginForm=this.formBuilder.group({
      email:['',[Validators.required, Validators.email]],
      password:['',[Validators.required,]]
    });
    if(localStorage.getItem('token')=='abcdefghijklmnopqrstuvwxyz')
    {
      this.ifLogin='In';
      console.log('The User is Logged In');
    }
    else
    {
      this.ifLogin='OUT';
      console.log('The User is Logged OUT');
    }
  }
  get email ()
  {
    return this.loginForm.get('email');
  }
  get password ()
  {
    return this.loginForm.get('password');
  }
  onSubmit(){
    console.log(this.loginForm.value);
    
    this.http.get<any>('http://localhost:3000/posts').subscribe(
      (res) => {
        console.log(res);
        const user = res.find((a:any)=>{
          if(a.email===this.loginForm.value.email && a.password===this.loginForm.value.password)
          {
            console.log(this.id=a.id);
            console.log(this.role=a.role);
          }
          return a.email===this.loginForm.value.email && a.password===this.loginForm.value.password
        });
        if(user)
        {
          this.loginForm.reset();
          localStorage.setItem('token','abcdefghijklmnopqrstuvwxyz');
          localStorage.setItem('role',this.role);
          localStorage.setItem('id',this.id);
          this.router.navigate(['/home']);
          console.log(this.id);
        }
        else{
          alert("User Not Found")
        }

      },
      (err) => {
        console.log(err);
     },);
  }
}
