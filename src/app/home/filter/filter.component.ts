import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterModel } from '../register.model';
//import { RegisterModel } from '../home/register.model';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  RegisterModel:RegisterModel[];
  searchValue:string;
  id:string;
  adminRole:boolean=false;
  superviseRole:boolean=false;
  constructor(private http:HttpClient,private router:Router,private router1: ActivatedRoute) { }

  ngOnInit(): void {
    this.http.get('http://localhost:3000/posts/').subscribe((res:RegisterModel[])=>{
      this.RegisterModel=res;
      this.id=localStorage.getItem('id');
    });
    if(localStorage.getItem('role')=='admin')
    {
      this.adminRole=true;
      console.log('this.adminRole=true;');
    }
    else if(localStorage.getItem('role')=='supervisor')
    {
      this.superviseRole=true;
      console.log('this.superviseRole=true;');
    }
  }
  onSubmit()
  {
    localStorage.removeItem('isLogging');
    this.router.navigate(['/login']);
  }

}
