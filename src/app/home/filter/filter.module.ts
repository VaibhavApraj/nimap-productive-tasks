import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilterRoutingModule } from './filter-routing.module';
import { FilterComponent } from './filter.component';
import { FormsModule } from '@angular/forms';
import { SearchfilterPipe } from './searchfilter.pipe';


@NgModule({
  declarations: [
    FilterComponent,
    SearchfilterPipe
  ],
  imports: [
    CommonModule,
    FilterRoutingModule,
    FormsModule
  ]
})
export class FilterModule { }
