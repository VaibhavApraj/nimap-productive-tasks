import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  ifEditText:boolean=true;
  ifEditInput:boolean=false;
  id:string;
  text:string;
  //diss:boolean=true;
@Input() public parentData;
//@Input('parentData') public name;
outputString="hii i am child";

@Output()  childEvent=new EventEmitter(); 
@Output()  childEvent1=new EventEmitter();
@ViewChild('name') inputName;
@ViewChild('InputText') InputText:ElementRef;
 
  constructor( private router:Router) { }

  ngOnInit(): void {
    console.log(this.parentData);
  }
  fireEvent()
  {
    this.childEvent.emit('Hey Apraj :'+this.outputString);
  }
  update()
  {
    if(this.InputText.nativeElement.value!='')
    {
      const valueInput = this.InputText.nativeElement.value
      console.log(valueInput);
      this.parentData[this.id]=valueInput;
      console.log('update',this.parentData);
      // this.ifEditText=true;
      // this.ifEditInput=false;
      var text = ((document.getElementById("custName") as HTMLInputElement).value);
      this.text=text;
      console.log(this.text,this.id);
      this.childEvent.emit(this.text);
      this.childEvent1.emit(this.id);
  
      var ref=document.getElementById('cancel');
      ref?.click();
      this.InputText.nativeElement.value = "";
      // Input='';
      this.router.navigate(['home/parent']);
    }
    else
    {
      alert("please enter name...")
    }
    
  }
  edit(i)
  {
    console.log('edit '+i);
    this.id=i;
    // this.ifEditText=false;
    // this.ifEditInput=true;
    // this.closebutton.nativeElement.click();
    //document.getElementById("closeModalButton").click();
  }

  myMethod(value){
    console.log(value);
    if(value!='')
    {
      // var ref1=document.getElementById('cancel');
      // ref1?.click();
     // this.diss=false;
    }
  
    }
}
