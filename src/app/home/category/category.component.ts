import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RegisterService } from 'src/app/sheard/register.service';
//import {RegisterService} from '../sheard/register.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(private activatedRoute:ActivatedRoute,private registerService:RegisterService) { }

  ngOnInit(): void {
    console.log("category");
    this.activatedRoute.queryParams.subscribe(res=>{
      let id=+localStorage.getItem('id');
      this.registerService.getData(id).subscribe(res1=>{
        console.log(res1);
      });
        console.log(res);
    });
  }

}
