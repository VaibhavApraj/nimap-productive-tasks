import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterService } from 'src/app/sheard/register.service';
import { RegisterModel } from '../register.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  fname:any;
  lname:any;
  id:any;
  color = 'green';
  registerData:RegisterModel;
  max20="abcdefghijklmnopqrstuvwxyz";
  constructor(private http:HttpClient,private router:Router,private router1: ActivatedRoute,private registerService:RegisterService) { }

  ngOnInit(): void {
    console.log(this.id=localStorage.getItem('id'));
    this.getData();
  }
  getData()
  {
    this.registerService.getData(this.id)
    .subscribe(
      (res) => {
         this.fname=res.fname;
         this.lname=res.lname;
         this.registerData=res;
        console.log(this.fname);
      },
      (err) => {
        console.log(err);
     },);
  }
}
