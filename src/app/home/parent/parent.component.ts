import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit  {

  public name:string="Vaibhav Apraj"
  public customer=['vaibhav','sanjay','apraj','swapnil','rahul','sushil','nikhil'];
   message:string;
   message1:string;
  constructor( private router:Router) {
   }
  ngOnInit(): void {
    console.log(this.message)
  }
  ngAfterViewChecked(): void {
    if(this.message!='' || this.message1!='')
    {
      console.log(this.message);
      console.log(this.message1);
      //this.customer.splice(this.customer.indexOf(this.message1), 1, this.message);
      this.customer[this.message1]=this.message;
      //this.customer;
      console.log(this.customer);
      //this.router.navigate(['home/parent']);
    }
  }
}