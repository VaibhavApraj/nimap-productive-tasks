import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForeachArrayComponent } from './foreach-array.component';

describe('ForeachArrayComponent', () => {
  let component: ForeachArrayComponent;
  let fixture: ComponentFixture<ForeachArrayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForeachArrayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForeachArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
