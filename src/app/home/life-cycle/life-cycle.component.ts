import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription,timer } from 'rxjs';

@Component({
  selector: 'app-life-cycle',
  templateUrl: './life-cycle.component.html',
  styleUrls: ['./life-cycle.component.css']
})
export class LifeCycleComponent implements OnInit, OnDestroy {

  ticks = 0;
  time=1;
  private timer;
  // Subscription object
  private sub: Subscription;
  constructor() {
    console.log("Life Cycle Component:Contructed");
  }
  ngOnChanges() {
    console.log("Life Cycle Component:ngOnChanges");
  }

  ngOnInit() {
    console.log("Life Cycle Component:ngOnInit");
      const ti = timer(2000,1000);    
    this.sub = ti.subscribe(t => {    
      this.time+=1;
        console.log("Tick",this.time);    
    }); 
  }
  tickerFunc(tick){
      console.log(this);
      this.ticks = tick
  }
  ngDoCheck() {
    console.log("Life Cycle Component:DoCheck");
  }
 
  ngAfterContentInit() {
    console.log("Life Cycle Component:ngAfterContentInit");
  }
 
  ngAfterContentChecked() {
    console.log("Life Cycle Component:AfterContentChecked");
  }
 
  ngAfterViewInit() {
    console.log("Life Cycle Component:AfterViewInit");
  }
 
  ngAfterViewChecked() {
    console.log("Life Cycle Component:AfterViewChecked");
  }
  ngOnDestroy(){
      console.log("Destroy timer");
      console.log("Life Cycle Component:ngOnDestroy");
      // unsubscribe here
      this.sub.unsubscribe();

  }


}
