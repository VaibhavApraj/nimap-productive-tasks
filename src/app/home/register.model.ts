export interface RegisterModel{
    fname:string;
    lname:string;
    email:string;
    password:string;
    username:string;
    phone:string;
    gender:string;
    role:string;
}