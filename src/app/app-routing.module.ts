import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
//import { ForeachArrayComponent } from './foreach-array/foreach-array.component';
import { HasRoleGuard } from './has-role.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { Role } from './role';

const routes: Routes = [
  {path:'home',canActivate: [AuthGuard],loadChildren:()=> import('./home/home.module').then((m)=>m.HomeModule)},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  //{path:'',redirectTo:'/login',pathMatch:'full'},
 //{ path: 'home/filter', loadChildren: () => import('./filter/filter.module').then(m => m.FilterModule) },
  //{ path: 'foreach', loadChildren: () => import('./foreach-array/foreach-array.module').then(m => m.ForeachArrayModule) },
  //{ path: 'category',canActivate: [AuthGuard,HasRoleGuard],data: { role: 'admin'}, loadChildren: () => import('./category/category.module').then(m => m.CategoryModule) },
  //  data: { roles: [Role.Admin] },
  //{ path: 'product',canActivate: [AuthGuard,HasRoleGuard],data: { role: 'supervisor' }, loadChildren: () => import('./product/product.module').then(m => m.ProductModule) },
   { path: '', redirectTo: '/home' ,pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
