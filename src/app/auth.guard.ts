import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor( private router: Router) {
    console.log(1);
  }
  canActivate(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree  {
    console.log(2,next,state);
      // if(localStorage.getItem('role')=='admin')
      // {
        //console.log(3,this.router.navigate(['/home/'+localStorage.getItem('id')]));
      //   console.log("admin",next.data['role']);
      //   if(next.data['role']=='admin')
      //   {
      //     return true;
      //   }else{
      //     return false;
      //   }
        
      // }
      // if(localStorage.getItem('role')=='supervisor')
      // {
      //   console.log("supervisor");
      //   if(next.data['role']=='supervisor')
      //   {
      //     return true;
      //   }else{
      //     return false;
      //   }
      // }
      // if(localStorage.getItem('role')=='user')
      // {
      //   c
      //   return true;
      // }

      if(localStorage.getItem('token'))
      {
        // console.log("user");
        // if(next.data['role']=='admin')
        //   {
        //     return true;
        //   }
        //   else if(next.data['role']=='supervisor')
        //   {
        //     return true;
        //   }
        //   else if(next.data['role']=='user')
        //   {
        //     return true;
        //   }

        //this.router.navigate(['/home/'+localStorage.getItem('id')]);
        return true;
      }
      this.router.navigate(['/login']);
      return false;
  }
  // 
  // setToken(token: string): void {
  //   localStorage.setItem('token', token);
  // }

  // getToken(): string | null {
  //   return localStorage.getItem('token');
  // }

  // isLoggedIn() {
  //   return this.getToken() !== null;
  // }

  // logout() {
  //   localStorage.removeItem('token');
  //   this.router.navigate(['']);
  // }

  // login({ email, password }: any): Observable<any> {
  //   if (email === 'admin@gmail.com' && password === 'admin123') {
  //     this.setToken('abcdefghijklmnopqrstuvwxyz');
  //     return of({ name: 'Admin', email: 'admin@gmail.com' });
  //   }
  //   return throwError(new Error('Failed to login'));
  // }
  

}
